# README #

Clone this repo to use the code for the relevant Hackster IO project.

### What is this repository for? ###

All the code here is for public non-commercial use. It is offered as a gesture of goodwill, to help people learn to code, and to teach them about any cool projects I've done. I will be adding twenty projects this year so watch me (with the icon above), and bit bucket will ping you when I add another one.

Upcoming projects will cover :

Neural Networks
Machine Learning
Swarm Intelligence
Robotics
Speech recognition
Object Avoidance and Navigation
Semantic Networks
Face recognition
OpenCv based image processing 
And lots lots more !

### How do I get set up? ###

There are several free IDE's I use, so I will endeavor to include all the necessary files for each IDE.

For most C# and C++ projects sue Visual Studio 2015 Community to build the code etc :

https://www.visualstudio.com/downloads/

For Arduino Use the Visual Micro Plugin for Visual Studio :

http://www.visualmicro.com/

Arduino IDE :

https://www.arduino.cc/en/Main/Software

To Run Arduino Sketches

1. Download the source from this repo.
2. Connect your 101 Arduino,
3. Run the Arduino 101 IDE,  OR use Visual Studio community 2015 with the Arduino Micro Code plugin
4. Build the source
5. Upload the sketch to the 101
6. Run the Serial Monitor with Baud rate of 9600


### Contribution guidelines ###
Please contact me below....

### Who do I talk to? ###
I own everything here, email me with any fixes, bugs, suggestions or new functionality ! 

Email : marcusobrien@yahoo.com
My Web Site : [http://www.roboticsfordreamers.com](Link URL)
My Blog : [http://www.roboticsfordreamers.com/single-post/2016/06/08/Grand-Plans-My-Robotic-Vision](Link URL)
My Engineering Blog : [http://www.electricalengineeringschools.org/author/mobrien](Link URL)
My Code Project Articles : [https://www.codeproject.com/Members/marcus-obrien](Link URL)
My Linked In : [https://ca.linkedin.com/in/marcus-obrien-70b5a688](Link URL)